import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { DetaildModule } from './detail/detail.module';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { HttpClientInMemoryWebApiModule } from 'angular-in-memory-web-api';
import { InMemoryDataService } from './core/services/in-memory-data.service';
import {
  MatToolbarModule, MatCardModule,
  MatIconModule, MatButtonModule, MatListModule,
  MatCheckboxModule,
  MatMenuModule,
  MatFormFieldModule, MatInputModule,
  MatSelectModule, MatTooltipModule, MatDialogModule,
  MatProgressSpinnerModule
} from '@angular/material';
import { MatSidenavModule } from '@angular/material/sidenav';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatTableModule } from '@angular/material/table';
import { DashboardComponent } from './layout/dashboard/dashboard.component';

@NgModule({
  declarations: [
    AppComponent,
    DashboardComponent,
  ],
  imports: [
    CommonModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    DetaildModule,
    FormsModule,
    MatToolbarModule, MatSidenavModule,
    MatListModule, MatCardModule,
    MatIconModule, MatButtonModule,
    MatCheckboxModule, MatTableModule,
    MatMenuModule, MatFormFieldModule,
    MatInputModule, MatSelectModule,
    MatTooltipModule,
    MatDialogModule,
    MatProgressSpinnerModule,
    HttpClientModule,
    ReactiveFormsModule,
    HttpClientInMemoryWebApiModule.forRoot(InMemoryDataService),
  ],
  providers: [InMemoryDataService],
  bootstrap: [AppComponent]
})
export class AppModule { }
