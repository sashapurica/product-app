import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './detail/components/home/home.component';
import { DashboardComponent } from './layout/dashboard/dashboard.component';
import { CreateProductComponent } from './detail/components/create-product/create-product.component';
import { DetailProductComponent } from './detail/components/detail-product/detail-product.component';


const routes: Routes = [
  {
    path: '', component: DashboardComponent,
    children: [
      { path: 'home', component: HomeComponent },
      { path: 'newProduct', component: CreateProductComponent },
      { path: 'detailProduct/:id/:name/:description/:price/:stock/:available/:type', component: DetailProductComponent }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
