export interface Product {
    id: string;
    name: string;
    description: string;
    type: string;
    price: number;
    stock: number;
    available: boolean;
}
