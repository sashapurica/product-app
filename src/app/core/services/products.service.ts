import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { environment } from './../../../environments/environment';
import { Observable } from 'rxjs';
import * as uuid from 'uuid';

@Injectable({
  providedIn: 'root'
})
export class ProductsService {

  constructor(
    private http: HttpClient
  ) { }

  public getProducts(): Observable<any> {
    return this.http.get(environment.appURL.getProducts);
  }

  public postProduct(product) {
    product.id = uuid.v4();
    return this.http.post(environment.appURL.getProducts, product);
  }

  public updateProduct(product) {
    return this.http.post(environment.appURL.getProducts, product);
  }
}
