import { Injectable } from '@angular/core';

import { InMemoryDbService } from 'angular-in-memory-web-api';
import * as uuid from 'uuid';

@Injectable({
  providedIn: 'root'
})
export class InMemoryDataService implements InMemoryDbService {
  createDb() {
    const list = [
      {
        name: 'Brown eggs',
        type: 'dairy',
        description: 'Raw organic brown eggs in a basket',
        stock: 400,
        price: 28.1,
        available: true
      },
      {
        name: 'Sweet fresh stawberry',
        type: 'fruit',
        description: 'Sweet fresh stawberry on the wooden table',
        stock: 299,
        price: 29.45,
        available: true
      },
      {
        name: 'Asparagus',
        type: 'vegetable',
        description: 'Asparagus with ham on the wooden table',
        stock: 299,
        price: 18.95,
        available: true
      },
      {
        name: 'Green smoothie',
        type: 'dairy',
        description: 'Glass of green smoothie with quail egg\'s yolk, served with cocktail tube, green apple and baby spinach leaves over tin surface.', stock: 399,
        price: 17.68,
        available: false
      },
      {
        name: 'Raw legums',
        type: 'vegetable',
        description: 'Raw legums on the wooden table',
        stock: 299,
        price: 17.11,
        available: true
      },
      {
        name: 'Baking cake',
        type: 'dairy',
        description: 'Baking cake in rural kitchen - dough  recipe ingredients (eggs, flour, sugar) on vintage wooden table from above.',
        stock: 675,
        price: 11.14,
        available: false
      },
      {
        name: 'Pesto with basil',
        type: 'vegetable',
        description: 'Italian traditional pesto with basil, chesse and oil',
        stock: 299,
        price: 18.19,
        available: false
      },
      {
        name: 'Hazelnut in black ceramic bowl',
        type: 'vegetable',
        description: 'Hazelnut in black ceramic bowl on old wooden background. forest wealth. rustic style. selective focus',
        stock: 301,
        price: 27.35,
        available: true
      },
      {
        name: 'Fresh stawberry',
        type: 'fruit',
        description: 'Sweet fresh stawberry on the wooden table',
        stock: 399,
        price: 28.59,
        available: false
      },
      {
        name: 'Lemon and salt',
        type: 'fruit',
        description: 'Rosemary, lemon and salt on the table',
        stock: 299,
        price: 15.79,
        available: false
      },
      {
        name: 'Homemade bread',
        type: 'bakery',
        description: 'Homemade bread',
        stock: 301,
        price: 17.48,
        available: true
      },
      {
        name: 'Legums',
        type: 'vegetable',
        description: 'Cooked legums on the wooden table',
        stock: 399,
        price: 14.77,
        available: true
      },
      {
        name: 'Fresh tomato',
        type: 'vegetable',
        description: 'Fresh tomato juice with basil',
        stock: 903,
        price: 16.3,
        available: true
      },
      {
        name: 'Healthy breakfast',
        type: 'fruit',
        description: 'Healthy breakfast set. rice cereal or porridge with berries and honey over rustic wood background',
        stock: 350,
        price: 13.02,
        available: true
      },
      {
        name: 'Green beans',
        type: 'vegetable',
        description: 'Raw organic green beans ready to eat',
        stock: 300,
        price: 28.79,
        available: true
      },
      {
        name: 'Baked stuffed portabello mushrooms',
        type: 'bakery',
        description: 'Homemade baked stuffed portabello mushrooms with spinach and cheese',
        stock: 400,
        price: 20.31,
        available: true
      },
      {
        name: 'Strawberry jelly',
        type: 'fruit',
        description: 'Homemade organic strawberry jelly in a jar',
        stock: 600,
        price: 14.18,
        available: false
      },
      {
        name: 'Pears juice',
        type: 'fruit',
        description: 'Fresh pears juice on the wooden table',
        stock: 398,
        price: 19.49,
        available: true
      },
      {
        name: 'Fresh pears',
        type: 'fruit',
        description: 'Sweet fresh pears on the wooden table',
        stock: 398,
        price: 15.12,
        available: false
      },
      {
        name: 'Caprese salad',
        type: 'vegetable',
        description: 'Homemade healthy caprese salad with tomato mozzarella and basil',
        stock: 600,
        price: 16.76,
        available: false
      },
      {
        name: 'Oranges',
        type: 'fruit',
        description: 'Orange popsicle ice cream bars made from fresh oranges.  a refreshing summer treat.',
        stock: 274,
        price: 21.48,
        available: true
      },
      {
        name: 'Vegan food',
        type: 'vegetable',
        description: 'Concept of vegan food',
        stock: 299,
        price: 29.66,
        available: false
      },
      {
        name: 'Breakfast with muesli',
        type: 'dairy',
        description: 'Concept of healthy breakfast with muesli',
        stock: 299,
        price: 22.7,
        available: true
      },
      {
        name: 'Honey',
        type: 'bakery',
        description: 'Honey and honeycell on the table',
        stock: 299,
        price: 17.01,
        available: true
      },
      {
        name: 'Breakfast with cottage',
        type: 'fruit',
        description: 'Healthy breakfast with cottage cheese and strawberry',
        stock: 398,
        price: 14.05,
        available: true
      },
      {
        name: 'Strawberry smoothie',
        type: 'fruit',
        description: 'Glass of red strawberry smoothie with chia seeds, served with retro cocktail tube, fresh mint and strawberries over dark background',
        stock: 400,
        price: 28.86,
        available: true
      },
      {
        name: 'Strawberry and mint',
        type: 'fruit',
        description: 'Homemade muesli with strawberry and mint',
        stock: 299,
        price: 26.21,
        available: false
      },
      {
        name: 'Ricotta',
        type: 'dairy',
        description: 'Ricotta with berry and mint',
        stock: 398,
        price: 27.81,
        available: true
      },
      {
        name: 'Cuban sandwiche',
        type: 'bakery',
        description: 'Homemade traditional cuban sandwiches with ham pork and cheese',
        stock: 300,
        price: 18.5,
        available: true
      },
      {
        name: 'Granola',
        type: 'dairy',
        description: 'Glass jar with homemade granola and yogurt with nuts, raspberries and blackberries on wooden cutting board over white textile in day light',
        stock: 300,
        price: 29.97,
        available: false
      },
      {
        name: 'Smoothie with chia seeds',
        type: 'fruit',
        description: 'Glass of red strawberry smoothie with chia seeds, served with retro cocktail tube, fresh mint and strawberries over wooden table',
        stock: 900,
        price: 25.26,
        available: true
      },
      {
        name: 'Yogurt',
        type: 'dairy',
        description: 'Homemade yogurt with raspberry and mint',
        stock: 299,
        price: 27.61,
        available: false
      },
      {
        name: 'Sandwich with salad',
        type: 'vegetable',
        description: 'Vegan sandwich with salad, tomato and radish',
        stock: 398,
        price: 22.48,
        available: false
      },
      {
        name: 'Cherry',
        type: 'fruit',
        description: 'Cherry with sugar on old table',
        stock: 400,
        price: 14.35,
        available: false
      },
      {
        name: 'Raw asparagus',
        type: 'vegetable',
        description: 'Raw fresh asparagus salad with cheese and dressing',
        stock: 400,
        price: 22.97,
        available: false
      },
      {
        name: 'Corn',
        type: 'vegetable',
        description: 'Grilled corn on the cob with salt and butter',
        stock: 300,
        price: 13.55,
        available: true
      },
      {
        name: 'Vegan',
        type: 'vegan',
        description: 'Concept of healthy vegan eating',
        stock: 398,
        price: 28.96,
        available: true
      },
      {
        name: 'Fresh blueberries',
        type: 'fruit',
        description: 'Healthy breakfast. berry crumble with fresh blueberries, raspberries, strawberries, almond, walnuts, pecans, yogurt, and mint in ceramic plates over white wooden surface, top view',
        stock: 321,
        price: 21.01,
        available: false
      },
      {
        name: 'Smashed avocado',
        type: 'fruit',
        description: 'Vegan sandwiches with smashed avocado, tomatoes and radish. top view',
        stock: 450,
        price: 25.88,
        available: true
      },
      {
        name: 'Italian ciabatta',
        type: 'bakery',
        description: 'Italian ciabatta bread cut in slices on wooden chopping board with herbs, garlic and olives over dark grunge backdrop, top view',
        stock: 565,
        price: 15.18,
        available: false
      },
      {
        name: 'Rustic breakfast',
        type: 'dairy',
        description: 'Rustic healthy breakfast set. cooked buckwheat groats with milk and honey on dark grunge backdrop. top view, copy space',
        stock: 307,
        price: 21.00,
        available: false
      },
      {
        name: 'Sliced lemons',
        type: 'fruit',
        description: 'Heap of whole and sliced lemons and limes with mint in vintage metal grid box over old wooden table with turquoise wooden background. dark rustic style.',
        stock: 900,
        price: 27.14,
        available: true
      },
      {
        name: 'Plums',
        type: 'fruit',
        description: 'Yellow and red sweet plums',
        stock: 299,
        price: 19.18,
        available: true
      },
      {
        name: 'French fries',
        type: 'bakery',
        description: 'Homemade oven baked french fries with ketchup',
        stock: 400,
        price: 18.32,
        available: true
      },
      {
        name: 'Strawberries',
        type: 'fruit',
        description: 'Healthy breakfast set. rice cereal or porridge with fresh strawberry, apricots, almond and honey over white rustic wood backdrop, top view, \u0000',
        stock: 352,
        price: 15.13,
        available: true
      },
      {
        name: 'Ground beef meat burger',
        type: 'meat',
        description: 'Raw ground beef meat burger steak cutlets with seasoning on vintage wooden boards, black background',
        stock: 675,
        price: 11.73,
        available: true
      },
      {
        name: 'Tomatoes',
        type: 'fruit',
        description: 'Organic tomatoes made with love',
        stock: 675,
        price: 26.03,
        available: true
      },
      {
        name: 'Basil',
        type: 'vegetable',
        description: 'Concept of vegan food with basil',
        stock: 678,
        price: 15.19,
        available: true
      },
      {
        name: 'Fruits bouquet',
        type: 'fruit',
        description: 'Abstract citrus fruits bouquet on blue background',
        stock: 401,
        price: 18.18,
        available: false
      },
      {
        name: 'Peaches on branch',
        type: 'fruit',
        description: 'Peaches on branch with leaves and glasses with peach juice and limonade with ice cubes in aluminum tray over old metal table. dark rustic style. top view.',
        stock: 400,
        price: 25.62,
        available: false
      }
    ];
    // tslint:disable-next-line: no-string-literal
    list.forEach(product => product['id'] = uuid.v4() );
    return { list };
  }
}
