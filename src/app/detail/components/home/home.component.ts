import { Component, OnInit } from '@angular/core';

import * as _ from 'lodash';
import { MatDialog } from '@angular/material/dialog';

import { CreateProductComponent } from './../create-product/create-product.component';
import { ProductsService } from '../../../core/services/products.service';


@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})

export class HomeComponent implements OnInit {

  products: any;

  constructor(
    public dialog: MatDialog,
    private productsService: ProductsService) { }

  ngOnInit() {
    this.getProducts();
  }

  getProducts() {
    this.productsService.getProducts().subscribe(products => {
       this.products = products ;
       this.products = _.orderBy(this.products, ['name'], ['asc']);
      }
    );
  }

  openDialog(): void {
    const dialogRef = this.dialog.open(CreateProductComponent, {
      width: '450px',
    });

    dialogRef.afterClosed().subscribe(result => {
      this.getProducts();
    });
  }

}
