import { Component, OnInit, Input, SimpleChanges, OnChanges } from '@angular/core';

import { Product } from 'src/app/core/interfaces/product';
import * as _ from 'lodash';

@Component({
  selector: 'app-grouped-product',
  templateUrl: './grouped-product.component.html',
  styleUrls: ['./grouped-product.component.scss']
})
export class GroupedProductComponent implements OnInit, OnChanges {

  @Input() name: string;
  @Input() products: Array<Product>;

  private originalProducts: Array<Product>;

  constructor() {
  }

  ngOnInit() {
    this.originalProducts = this.products.map(a => ({ ...a }));
  }

  ngOnChanges(changes: SimpleChanges) {
    if (changes.products) {
      this.originalProducts = this.products.map(a => ({ ...a }));
    }
  }

  applyFilter(e: KeyboardEvent) {
    // tslint:disable-next-line:no-string-literal
    this.products = this.originalProducts.filter(item => item.name.indexOf(e.target['value']) !== -1);
  }


}
