import { Component, OnInit, Inject, Input } from '@angular/core';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';
import { ProductsService } from 'src/app/core/services/products.service';

import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Product  } from 'src/app/core/interfaces/product';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-detail-product',
  templateUrl: './detail-product.component.html',
  styleUrls: ['./detail-product.component.scss']
})
export class DetailProductComponent implements OnInit {

  productForm: FormGroup;
  submitted = false;
  minDate: number;
  product: any;

  constructor(
    private fb: FormBuilder,
    private router: Router,
    private route: ActivatedRoute,
    private productService: ProductsService ) {
      route.params.subscribe(parametros => {
        this.product = parametros;
      });
    }

  ngOnInit() {
    this.productForm = this.fb.group({
      id: ['', Validators.required],
      name: ['', Validators.required],
      description: ['', Validators.required],
      type: ['', Validators.required],
      stock: ['', Validators.required],
      price: ['', Validators.required],
      available: ['', Validators.required]
    });
    this.setvalueEdit(this.product);
  }

  get f() { return this.productForm.controls; }

  onSubmit() {
    this.submitted = true;
    if (this.productForm.invalid) {
      return this.productForm;
    } else if (this.productForm.valid) {
      this.updateProduct(this.productForm.value);
    }
  }

  onNoClick(): void {
    this.router.navigate(['/home']);
  }

  updateProduct(product) {
    this.productService.updateProduct(product).subscribe(
      data => {
        this.onNoClick();
      }, error => {
        console.log('Error postEvent :: ' + error);
      }
    );

  }

  setvalueEdit(product) {
    this.productForm.controls.id.setValue(product.id);
    this.productForm.controls.name.setValue(product.name);
    this.productForm.controls.description.setValue(product.description);
    this.productForm.controls.type.setValue(product.type);
    this.productForm.controls.price.setValue(product.price);
    this.productForm.controls.stock.setValue(product.stock);
    this.productForm.controls.available.setValue(product.available);
}

}
