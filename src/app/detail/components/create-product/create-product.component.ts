import { Component, OnInit, Inject } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ProductsService } from 'src/app/core/services/products.service';

export interface DialogData {
  message: string;
}

@Component({
  selector: 'app-create-product',
  templateUrl: './create-product.component.html',
  styleUrls: ['./create-product.component.scss']
})
export class CreateProductComponent implements OnInit {

  productForm: FormGroup;
  submitted = false;
  minDate: number;

  constructor(
    public dialogRef: MatDialogRef<CreateProductComponent>,
    @Inject(MAT_DIALOG_DATA)
    public data: DialogData,
    private fb: FormBuilder,
    private productService: ProductsService
  ) { }

  ngOnInit() {
    this.productForm = this.fb.group({
      name: ['', Validators.required],
      description: ['', Validators.required],
      type: ['', Validators.required],
      stock: ['', Validators.required],
      price: ['', Validators.required],
      available: ['', Validators.required]
    });
  }

  onNoClick(): void {
    this.dialogRef.close(this.productForm.value);
  }

  get f() { return this.productForm.controls; }

  onSubmit() {
    this.submitted = true;
    if (this.productForm.invalid) {
      return this.productForm;
    } else if (this.productForm.valid) {
      this.postProduct(this.productForm.value);
    }
  }

  postProduct(product) {
    this.productService.postProduct(product).subscribe(
      data => {
        this.onNoClick();
      }, error => {
        console.log('Error postProduct :: ' + error);
      }
    );

  }
}
