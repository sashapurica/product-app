const baseURL = 'https://demo0034747.mockable.io'; // Ruta base

const appURL = {
  getProducts: baseURL + '/product/list',

};

export const environment = {
  production: false,
  baseURL,
  appURL
};
